package com.petrix.simpleshopping.Adapters;

import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.petrix.simpleshopping.Activities.MainActivity;
import com.petrix.simpleshopping.ImageUtils.ImageUtils;
import com.petrix.simpleshopping.Product.Product;
import com.petrix.simpleshopping.Product.Promotion;
import com.petrix.simpleshopping.R;

import java.util.ArrayList;
import java.util.List;

import static com.petrix.simpleshopping.Activities.MainActivity.activity;

public class ProductAdapter extends ArrayAdapter<String> {

    private List<String> barcodeList;
    private TextView textView;

    public ProductAdapter(@NonNull Context context, ArrayList<String> barcodeList) {
        super(context, 0, barcodeList);
        this.barcodeList = barcodeList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null) {
            listItem = LayoutInflater.from(activity).inflate(R.layout.product_row, parent, false);
        }

        ImageView itemImage = listItem.findViewById(R.id.productImage);
        TextView itemDescription = listItem.findViewById(R.id.firstDescription);
        TextView promoDescription = listItem.findViewById(R.id.promoDescription);
        Button minusButton = listItem.findViewById(R.id.minusButton);
        TextView quantityTextView = listItem.findViewById(R.id.quantityText);
        Button plusButton = listItem.findViewById(R.id.plusButton);
        final TextView priceTextView = listItem.findViewById(R.id.priceTextView);
        Button deleteButton = listItem.findViewById(R.id.deleteButton);

        Pair<Product, Integer> item = MainActivity.getShoppingCart().get(barcodeList.get(position));
        final Product product = item.first;
        final int quantity = item.second;

        List<Promotion> promotions = product.getPromotions();

        itemImage.setImageBitmap(ImageUtils.base64ToBitmap(product.getImage()));
        itemDescription.setText(product.getName());

        Float price = quantity * Float.valueOf(product.getPrice());

        if (promotions.size() > 0) {
            promoDescription.setText(promotions.get(0).getDesc() + " " + promotions.get(0).getDiscount() + "%");
            price = price - price * Float.valueOf(promotions.get(0).getDiscount()) / 100;
        }
        quantityTextView.setText(String.valueOf(quantity));

        priceTextView.setText(String.format("%.2f", price));

        if (quantity == 1) {
            minusButton.setEnabled(false);
        } else {
            minusButton.setEnabled(true);
        }

        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quantity - 1 == 0) {
                    MainActivity.removeElement(product.getBarcode());
                } else {
                    MainActivity.addElement(product.getBarcode(), new Pair<>(product, quantity - 1));
                }
                MainActivity.loadListView();
            }
        });

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.addElement(product.getBarcode(), new Pair<>(product, quantity + 1));
                MainActivity.loadListView();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.removeElement(product.getBarcode());
                MainActivity.loadListView();
            }
        });

        return listItem;
    }

}
