package com.petrix.simpleshopping.Adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.petrix.simpleshopping.R;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class ShopingListAdapter extends RecyclerView.Adapter<ShopingListAdapter.ShopingListViewHolder> {
    private ArrayList<DesiredProduct> mDataset;


    private Context mContext;


    public class ShopingListViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        /*public CircleImageView productIcon;
        public   TextView productDescription;
        public TextView  promoDescription;

        */
        public Button btPlus;
        public Button btMinus;
        public Button btBin;
        public TextView tvQuantity;
        public int viewId;
        public EditText etItem;

        public ShopingListViewHolder(View itemView) {

            super(itemView);

            /*
            productDescription =itemView.findViewById(R.id.firstDescription);
            promoDescription = itemView.findViewById(R.id.promoDescription);
            productIcon  = itemView.findViewById(R.id.productIcon);
            */

            btPlus = itemView.findViewById(R.id.btPlus);
            btMinus = itemView.findViewById(R.id.btMinus);
            btBin = itemView.findViewById(R.id.btBin);
            tvQuantity = itemView.findViewById(R.id.tvQuantity);
            btPlus.setTag(mDataset.size());
            etItem = itemView.findViewById(R.id.firstDescription);

            etItem.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int pos = getLayoutPosition();
                    mDataset.get(pos).setDescription(s.toString());
                }
            });
            btBin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getLayoutPosition();
                    mDataset.remove(pos);
                    notifyItemRemoved(pos);
                }
            });
            btPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d(TAG, "onClick: click on" + getLayoutPosition());
                    int position = getLayoutPosition();
                    mDataset.get(position).addQuantity();

                    Log.d(TAG, "onClick: try to notify " + getLayoutPosition());
                    notifyItemChanged(position);

                }
            });
            btMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int position = getLayoutPosition();
                    mDataset.get(position).subQuantity();
                    notifyItemChanged(position);
                }
            });
        }

    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ShopingListAdapter(Context mContext) {
        this.mContext = mContext;

        mDataset = new ArrayList<DesiredProduct>();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ShopingListAdapter.ShopingListViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                       int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.shopping_list_item, viewGroup, false);
        view.setTag(mDataset.size());
        ShopingListViewHolder holder = new ShopingListViewHolder(view);

        return holder;
    }

    public void addShoppingListItem() {

        DesiredProduct item = new DesiredProduct();
        item.setDescription("");
        item.setQuantity(1);
        mDataset.add(item);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ShopingListViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //      holder.textView.setText(mDataset.get(position));
 /*
        Product product = mDataset.get(position);
        holder.productDescription.setText(product.getDesc());
        holder.promoDescription.setText(product.getPromotions().get(0).getDesc());
  */
        holder.tvQuantity.setText("" + mDataset.get(position).getQuantity());
        holder.etItem.setText(mDataset.get(position).getDescription());
        Log.d(TAG, "onBindViewHolder: enter here on " + position);
    }


    // Return the size of your dataset (invoked by the layout manager)
    public void setDataset(ArrayList<DesiredProduct> data) {
        this.mDataset = data;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}