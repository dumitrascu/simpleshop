package com.petrix.simpleshopping.Adapters;

public class DesiredProduct {

    private  int quantity;
    private String description;
    public  DesiredProduct(){

    }

    public  void setDescription(String value){
        description = value;
    }
    public void setQuantity(int value){
        quantity=value;
    }
    public  int getQuantity(){
        return  quantity;
    }
    public  String getDescription(){
        return  description;
    }
    public void addQuantity(){
        quantity = quantity +1;

    }
    public  void subQuantity(){
        quantity -= 1;
        if (quantity < 1){
            quantity = 1;
        }
    }
}
