package com.petrix.simpleshopping.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Pair;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.petrix.simpleshopping.Product.Product;
import com.petrix.simpleshopping.R;

import java.util.HashMap;

public class FinishActivity extends AppCompatActivity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        String shoppingListText = generateText();
        imageView = findViewById(R.id.imageView);

        generateQR(shoppingListText);
    }

    private void generateQR(String shoppingListText) {
        MultiFormatWriter multiFormatWriter=new MultiFormatWriter();
        try {
            BitMatrix bitMatrix=multiFormatWriter.encode(shoppingListText, BarcodeFormat.CODABAR.QR_CODE, 500,500);
            BarcodeEncoder barcodeEncoder= new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            imageView.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    private String generateText() {
        HashMap<String, Pair<Product, Integer>> shoppingCart = MainActivity.getShoppingCart();

        String result = "";

        for (String key : shoppingCart.keySet()) {
            Product product = shoppingCart.get(key).first;
            int quantity = shoppingCart.get(key).second;
            for (int i = 0; i < quantity; ++i) {
                result = result.concat(product.getBarcode() + "\n");
            }
        }

        return result;
    }
}
