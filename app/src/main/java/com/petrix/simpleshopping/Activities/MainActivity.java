package com.petrix.simpleshopping.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.petrix.simpleshopping.Adapters.ProductAdapter;
import com.petrix.simpleshopping.Product.Product;
import com.petrix.simpleshopping.Product.Promotion;
import com.petrix.simpleshopping.Product.Related;
import com.petrix.simpleshopping.R;
import com.petrix.simpleshopping.TalkToServer.ClientSide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    private static final int REQUEST_CODE = 69;

    public static MainActivity activity;
    public static Context context;

    private ZXingScannerView scannerView;

    private Button scanButton;
    private Button addButton;
    private EditText barcodeEditView;

    private static HashMap<String, Pair<Product, Integer>> shoppingCart = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = this;
        context = this;

        checkAndRequestPermissions();
        initControls();

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        shoppingCart = new LinkedHashMap<>();
    }

    private void checkAndRequestPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CODE);
        }
    }

    private void initControls() {
        scanButton = findViewById(R.id.scanButton);
        addButton = findViewById(R.id.addButton);
        barcodeEditView = findViewById(R.id.barcodeEditText);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    class ZxingScannerResultHandler implements ZXingScannerView.ResultHandler {
        @Override
        public void handleResult(Result result) {
            String barcode = result.getText();
            ClientSide.getInstance().sendBarcode(barcode);

            setContentView(R.layout.activity_main);
            scannerView.stopCamera();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onScanClicked(View view) {
        scannerView = new ZXingScannerView(this);

        scannerView.setResultHandler(new ZxingScannerResultHandler());
        setContentView(scannerView);
        scannerView.startCamera();
    }

    public void onAddClicked(View view) {
        String barcode = barcodeEditView.getText().toString();
        ClientSide.getInstance().sendBarcode(barcode);
    }

    public void onTodoClicked(View view) {
        Intent intent = new Intent(context, ShoppingListActivity.class);
        startActivity(intent);
    }

    public void onFinishClicked(View view) {
        startActivity(new Intent(this, FinishActivity.class));
    }

    public static HashMap<String, Pair<Product, Integer>> getShoppingCart() {
        return shoppingCart;
    }

    public static void addElement(String key, Pair<Product, Integer> product) {
        shoppingCart.put(key, product);
    }

    public static void removeElement(String key) {
        shoppingCart.remove(key);
    }

    public static Pair<Product, Integer> getProduct(String key) {
        if (shoppingCart.containsKey(key)) {
            return shoppingCart.get(key);
        }
        return null;
    }

    public static void loadListView() {

        MainActivity.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                TextView totalTextView = MainActivity.activity.findViewById(R.id.totalTextView);
                ListView listView = MainActivity.activity.findViewById(R.id.shoppingCartListView);

                if (listView != null) {

                    Set<String> keys = shoppingCart.keySet();
                    ArrayList<String> barcodeList = new ArrayList<>(keys);
                    ProductAdapter adapter = new ProductAdapter(MainActivity.activity, barcodeList);
                    listView.setAdapter(adapter);


                    float total = 0;
                    for (String key : shoppingCart.keySet()) {
                        Pair<Product, Integer> product = shoppingCart.get(key);

                        float price = product.second * Float.valueOf(product.first.getPrice());
                        List<Promotion> promotions = product.first.getPromotions();

                        if (promotions.size() > 0) {
                            float procent = Float.valueOf(promotions.get(0).getDiscount());

                            price = price - procent * price / 100;
                        }

                        total = total + price;
                    }

                    totalTextView.setText("Total: " + String.format("%.2f", total) + " RON");

                    SharedPreferences preferences = MainActivity.context.getSharedPreferences("shoppingList", 0);

                    float buget = preferences.getFloat("BUGET", 0);

                    if (buget > 0) {
                        float diferena = buget - total;

                        if (diferena < 0) {
                            popUpAd(Math.abs(diferena));
                        }
                    }
                }
            }
        });
    }

    private static void popUpAd(final float diferenta) {

        MainActivity.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("ATENTIE : BUGET DEPASIT");
                alertDialog.setMessage("Ai depasit bugetul cu " + String.format("%.2f", diferenta) + " lei.");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });
    }
}


