package com.petrix.simpleshopping.Activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.petrix.simpleshopping.Adapters.DesiredProduct;
import com.petrix.simpleshopping.Adapters.ShopingListAdapter;
import com.petrix.simpleshopping.R;

import java.util.ArrayList;

public class ShoppingListActivity extends AppCompatActivity {

    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView mListRecycleView;
    private ShopingListAdapter mAdapter;
    private Button mBtAddItem;
    private ArrayList<DesiredProduct> desiredProducts;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shopping_list);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initComponents();
        initListeners();
    }

    public void initListeners() {
        mBtAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.addShoppingListItem();
                mListRecycleView.setAdapter(mAdapter);
            }
        });
    }

    public void initComponents() {
        mAdapter = new ShopingListAdapter(this);
        mListRecycleView = findViewById(R.id.listRecycleView);
        layoutManager = new LinearLayoutManager(this);
        mListRecycleView.setLayoutManager(layoutManager);
        mBtAddItem = (Button) findViewById(R.id.btAddItem);

        mListRecycleView.setItemViewCacheSize(20);
        mListRecycleView.setDrawingCacheEnabled(true);
        mListRecycleView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        desiredProducts = new ArrayList<DesiredProduct>();

        SharedPreferences pref = getApplicationContext().getSharedPreferences("shoppingList", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();

        int nrElements = pref.getInt("ListSize", 0);

        for (int i = 0; i < nrElements; ++i) {
            int quantity = pref.getInt("quantity" + i, 0);
            String description = pref.getString("description" + i, "");

            DesiredProduct product = new DesiredProduct();
            product.setDescription(description);
            product.setQuantity(quantity);
            desiredProducts.add(product);
        }

        EditText editText = findViewById(R.id.bugetEditText);
        float buget = pref.getFloat("BUGET", 0);
        if (buget > 0) {
            editText.setText(String.valueOf(buget));
        }

        mAdapter.setDataset(desiredProducts);

    }

    @Override
    protected void onResume() {
        super.onResume();


        mListRecycleView.setAdapter(mAdapter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        int size = desiredProducts.size();
        for (int i = 0; i < size; ++i) {
            View viewAtPosI = mListRecycleView.getLayoutManager().findViewByPosition(i);
            EditText et = viewAtPosI.findViewById(R.id.firstDescription);
            Log.d("test", "onDestroy: " + et.getText().toString());
            desiredProducts.get(i).setDescription(et.getText().toString());

        }


        SharedPreferences pref = getApplicationContext().getSharedPreferences("shoppingList", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();


        int nrElements = desiredProducts.size();
        editor.putInt("ListSize", nrElements);
        for (int i = 0; i < nrElements; ++i) {
            int quantity = desiredProducts.get(i).getQuantity();
            String descp = desiredProducts.get(i).getDescription();

            editor.putInt("quantity" + i, quantity);
            editor.putString("description" + i, descp);

            EditText bugetEditText = findViewById(R.id.bugetEditText);
            String text = bugetEditText.getText().toString();
            Float buget = Float.valueOf(text);
            if (buget > 0) {
                editor.putFloat("BUGET", buget);
            }
        }
        editor.commit();

    }
}
