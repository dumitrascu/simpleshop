package com.petrix.simpleshopping.TalkToServer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.petrix.simpleshopping.Activities.MainActivity;
import com.petrix.simpleshopping.Product.Product;
import com.petrix.simpleshopping.Product.Related;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ClientSide {

    private static final String TAG = ClientSide.class.getName();

    public static ClientSide getInstance() {
        if (client == null) {
            client = new ClientSide();
        }
        return client;
    }

    private static ClientSide client = null;
    private static final String SERVER_IP = "192.168.87.69";
    private static final int PORT = 1515;

    private ClientSide() {
    }

    public void sendBarcode(final String barcode) {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Socket socket = new Socket();
                    socket.connect(new InetSocketAddress(SERVER_IP, PORT), 3000);

                    DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
                    outputStream.write(barcode.getBytes());
                    outputStream.flush();

                    DataInputStream inputStream = new DataInputStream(socket.getInputStream());

                    byte[] dataLengthBuffer = new byte[8];
                    inputStream.read(dataLengthBuffer, 0, dataLengthBuffer.length);
                    int dataLength = Integer.valueOf(new String(dataLengthBuffer));

                    byte[] buffer = new byte[dataLength];
                    for (int i=0; i<buffer.length; ++i) {
                        inputStream.read(buffer, i, 1);
                    }

                    String response = new String(buffer).substring(0, buffer.length);

                    socket.close();
                    Log.d(TAG, "run: response = " + response);
                    if (!response.equals("{}")) {

                        Gson gson = new Gson();
                        Map<String, Product> decoded = gson.fromJson(response, new TypeToken<Map<String, Product>>() {
                        }.getType());

                        Product product = decoded.get("product");

                        Pair<Product, Integer> result = MainActivity.getProduct(product.getBarcode());
                        if (result == null) {
                            MainActivity.addElement(product.getBarcode(), new Pair<>(product, 1));
                        }

                        boolean alcohol = false;

                        if (product.getName().toLowerCase().contains("bere")) {
                            alcohol = true;
                        } else {
                            alcohol = false;
                        }

                        boolean flag = true;

                        List<Related> relatedList = product.getRelated();
                        if (relatedList.size() > 0) {
                            popUpAd(relatedList.get(0), alcohol);
                            flag = false;
                        }
                        if (flag && alcohol){
                            alcoholPopUp();
                        }

                        Log.d(TAG, "run: ceva = "+response);
                        for(int i = 0 ; i < 10000000 ; ++i){
                            int x= 0;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                MainActivity.loadListView();
            }

            private void popUpAd(final Related related, final boolean alcohol) {

                MainActivity.activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                        alertDialog.setTitle("Recomandare: " + related.getName());
                        StringBuilder builder = new StringBuilder();
                        builder.append(related.getDescRel() + " la pretul de " + related.getPrice() + " lei.");
                        if(alcohol) {
                            builder.append("\n" + "ATENTIE: acest produs este comercializat DOAR persoanelor peste 18 ani.");
                        }
                        alertDialog.setMessage(builder.toString());
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                });
            }

            private void alcoholPopUp() {

                MainActivity.activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                        alertDialog.setTitle("ATENTIE!");
                        alertDialog.setMessage("Acest produs este comercializat DOAR persoanelor peste 18 ani.");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                });
            }
        });


        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}