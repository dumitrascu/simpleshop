package com.petrix.simpleshopping.Product;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Related {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("desc_rel")
    @Expose
    private String descRel;

    /**
     * No args constructor for use in serialization
     *
     */
    public Related() {
    }

    /**
     *
     * @param descRel
     * @param price
     * @param name
     */
    public Related(String name, String price, String descRel) {
        super();
        this.name = name;
        this.price = price;
        this.descRel = descRel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescRel() {
        return descRel;
    }

    public void setDescRel(String descRel) {
        this.descRel = descRel;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("price", price).append("descRel", descRel).toString();
    }

}