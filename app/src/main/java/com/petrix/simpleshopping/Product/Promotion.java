package com.petrix.simpleshopping.Product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Promotion {

    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("desc")
    @Expose
    private String desc;

    /**
     * No args constructor for use in serialization
     *
     */
    public Promotion() {
    }

    /**
     *
     * @param discount
     * @param desc
     */
    public Promotion(String discount, String desc) {
        super();
        this.discount = discount;
        this.desc = desc;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("discount", discount).append("desc", desc).toString();
    }

}
