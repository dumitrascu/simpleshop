package com.petrix.simpleshopping.Product;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Product {

    @SerializedName("barcode")
    @Expose
    private String barcode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("promotions")
    @Expose
    private List<Promotion> promotions = null;
    @SerializedName("related")
    @Expose
    private List<Related> related = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Product() {
    }

    /**
     *
     * @param image
     * @param promotions
     * @param related
     * @param price
     * @param name
     * @param weight
     * @param barcode
     * @param desc
     */
    public Product(String barcode, String name, String price, String desc, String weight, String image, List<Promotion> promotions, List<Related> related) {
        super();
        this.barcode = barcode;
        this.name = name;
        this.price = price;
        this.desc = desc;
        this.weight = weight;
        this.image = image;
        this.promotions = promotions;
        this.related = related;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<Promotion> promotions) {
        this.promotions = promotions;
    }

    public List<Related> getRelated() {
        return related;
    }

    public void setRelated(List<Related> related) {
        this.related = related;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("barcode", barcode).append("name", name).append("price", price).append("desc", desc).append("weight", weight).append("image", image).append("promotions", promotions).append("related", related).toString();
    }

}
